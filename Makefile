SHELL = /bin/bash

HOOK=/lib/systemd/system-sleep/systemd_hook_wakeup.sh
SUDOERS=/etc/sudoers.d/laptop_wakeup


all: run


run:
	bash src/wakeup.sh


install: chk-root $(HOOK) $(SUDOERS)



# Create a systemd hook script
$(HOOK):
	SCRIPTDIR='$$SCRIPTDIR' RUNUSER='$$RUNUSER' I3SOCK='$$I3SOCK' \
	CONF_RUNUSER="$(USER)" CONF_SCRIPTDIR="$(PWD)/src" \
	  envsubst < $(PWD)/src/systemd_hook_wakeup.sh \
	  | sudo tee $(HOOK) >/dev/null
	sudo chmod a+x $(HOOK)



# Create a sudoers file for the executing user
$(SUDOERS):
	echo "$(USER) ALL = NOPASSWD: /usr/bin/openfortivpn, /usr/bin/skill openfortivpn" \
	  | sudo tee $(SUDOERS) >/dev/null
	sudo chown 0:0 $(SUDOERS)



.PHONY: chk-root
chk-root:
ifeq ("$(shell id -u)", "0")
	$(error "Don't run as root")
endif



clean:
	sudo rm -f $(HOOK) $(SUDOERS)
