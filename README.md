# turbovnc-autoreconnect

The purpose of this program is to have your turbovnc clients automatically reconnected when you resume your laptop.

It is quite specific to my condition: you can configure either no vpn
or openfortivpn. If you use wireguard, you need to make adjustments.


## REQUIREMENTS
```
apt-get install sshpass openssh-client autossh openfortivpn netcat i3 i3status \
                bash x11-xkb-utils turbovnc openjdk-11-jdk jq openjdk-11-jdk
```


## CONFIGURATION

### systemd configuration file path
In `Makefile`, verify the systemd `system-sleep` path (default value: Ubuntu's path):
```
HOOK=/lib/systemd/system-sleep/systemd_hook_wakeup.sh
```

### sudoers configuration file path
In `Makefile`, verify the directory for sudoers:
```
SUDOERS=/etc/sudoers.d/laptop_wakeup
```

## secrets

### secrets/openfortivpn.conf
Create a regular openfortivpn config:
```
host =
port =
username =
password =
trusted-cert =
```

### secrets/wakeup_conf.json
```
{
    "ssh_generalopts": "-o Compression=yes -o ServerAliveInterval=240 -o ServerAliveCountMax=3 -o StrictHostKeyChecking=no -o ExitOnForwardFailure=yes ",
    "use_openfortivpn": "true",
    "hosts": {
	"ssh_host0_behind_vpn": {
	    "host": "",                                                      # config
	    "ssh_tunnelopts": "-L5903:localhost:590$turbovnc_local_display", # replace 5903 with the turbovnc server port on this host
	    "ssh_opts": "-i ~/.ssh/host0_priv_key ssh-remote-user@$host",    # config ($host will be interpolated)
	},
	"ssh_host1_behind_vpn": {
            ...
	},
	...
    }
}
```

### secrets/pass/<hostname>.sh
For each key in `hosts` above, create a file with the following contents, and
read protect it from group and others.
```
export ssh_passphrase="<passphrase>"
export turbovnc_password="<password>"
```


## ADD STATUS TO i3status bar
Modify the `bar` section of `~/.config/i3/config`
```
bar {
        status_command /path/to/turbovnc-autoreconnect/src/i3status.sh
}

```
Set `output_format` in your `i3status.conf`:
```
general {
	output_format = "i3bar"
	...
```


## TESTING THE SETUP
Add `--debug` in need of trace, but beware it will trace your passwords too.
```
bash src/wakeup.sh
```
You can also run `make`.



## INSTALL
```
make install
```

This will install `src/systemd_hook_wakeup.sh` in the systemd folder
`system-sleep` (see Makefile). That script calls  `src/wakeup.sh` upon laptop
resume.



## CAVEATS

 - The program assumes ssh key athentication, and won't work with simple
ssh passwords.

 - The program runs:
```
sudo skill openfortivpn
skill autossh
skill vncviewer
```
so if you have some valueable processes running with those programs, be aware.

 - I've strewn a number of sleeps here and there, probably causing race conditions and what not.


## SECURITY ISSUE
All your security relies on keeping the files under `secrets/` safe,
as it contains all your credentials.

Also, running `wakeup.sh --debug` with trace will show your passwords.


## PROBLEMS
Gnome's notification daemon has a very low limit, which means we cannot send updates using notifications:
```
GDBus.Error:org.freedesktop.Notifications.MaxNotificationsExceeded: Exceeded maximum number of notifications
```
Look into how to replace Gnome's notification daemon with notify-osd or dunst.

For now, notifications have been disabled (see function `log` in `src/wakeup.sh`).


## TODO
It would be nice to use a better secrets handling. Currently, the
secrets reside in the `secrets` directory, and you need to remember to
restrict the permissions here.
