#!/usr/bin/env bash
###############################################################################
set -eEu -o pipefail

# Log to terminal if tty
if [ -t 1 ] ; then
    LOGFILE=
else
    LOGFILE=/tmp/pm."$USER".log
fi


MODE=initiate
DEBUG=
START_VNC=1
HOST=

SCRIPT=$(readlink -f ${BASH_SOURCE[0]})
# Detect the directory name of this script and derive secrets sibling dir
selfdir="$( cd "$( dirname "${SCRIPT}" )" >/dev/null 2>&1 && pwd )"
secrets_dir="${selfdir}/../secrets"
vncviewer=/opt/TurboVNC/bin/vncviewer
ssh_generalopts=$(jq -r .ssh_generalopts < "${secrets_dir}"/wakeup_conf.json)
turbovnc_local_display=2



#### PARSE ARGS

options=$(getopt -o kxXsdhl  -- "$@")
[ $? -eq 0 ] || {
    echo "Incorrect options provided"
    exit 1
}
eval set -- "$options"
while true; do
    case "$1" in
    -k|-x)
        MODE=terminate
        ;;
    -s)
        MODE=status
        ;;
    -X)
        START_VNC=
        ;;
    -d)
        DEBUG=1
        ;;
    -l)
        MODE=list
        ;;
    -h)
        MODE=help
        ;;
    --)
        shift
	HOST="${1:-}"
        break
        ;;
    esac
    shift
done



# Useful for debugging but beware - trace contains cleartext passwords
if [ -n "$DEBUG" ]; then
   set -x
   LOGFILE=
fi



#### FUNCTIONS

function log() {
    echo "[$(date)] $1" >&2
    #notify-send -t 700 "$1"
}



function kill_vnc() {
    skill vncviewer
    sleep 2
    local PIDS="$(pgrep --uid $(id -u) -f '^.*TurboVNC.*java.*$' || true)"
    if [ -n  "$PIDS" ]; then
	log "Force killing vncviewer ($PIDS)"
	kill -9 $PIDS
    fi
}



function connect_host() {
    local key="$1"

    # make sure we don't have leftover values
    local host=$(jq -r .hosts.'"'$key'"'.host < "${secrets_dir}"/wakeup_conf.json)
    local ssh_tunnelopts=$(jq -r .hosts.'"'$key'"'.ssh_tunnelopts < "${secrets_dir}"/wakeup_conf.json)
    local ssh_opts=$(jq -r .hosts.'"'$key'"'.ssh_opts < "${secrets_dir}"/wakeup_conf.json)
    local ssh_passphrase=$(jq -r .hosts.'"'$key'"'.ssh_passphrase < "${secrets_dir}"/wakeup_conf.json)
    local turbovnc_password=$(jq -r .hosts.'"'$key'"'.turbovnc_password < "${secrets_dir}"/wakeup_conf.json)

    # source $ssh_passphrase and $turbovnc_password
    #. "${secrets_dir}"/pass/"$key".sh || { log "cannot read ${secrets_dir}/pass/$key".sh; exit 1 ; }


    if [ -z "$ssh_passphrase" ]; then
	log "cannot find exported \$ssh_passphrase in ${secrets_dir}/pass/$key".sh
	exit 1
    fi

    if [ "$START_VNC" == 1 ]; then
	if [ -z "$turbovnc_password" ]; then
	    log "cannot find exported \$turbovnc_password in ${secrets_dir}/pass/$key".sh
	    exit 1
	fi
    fi

    echo >&2
    echo "**** connecting to $key..." >&2

    SSHPASS="${ssh_passphrase}"
    #sshfs_remote_dir=$(jq -r .hosts.'"'$key'"'.sshfs_remote_dir < "${secrets_dir}"/wakeup_conf.json)

    # substitute $host and $turbovnc_local_display
    export host
    export turbovnc_local_display
    ssh_opts="$(echo $ssh_opts | envsubst)"
    ssh_tunnelopts="$(echo $ssh_tunnelopts | envsubst)"

    # start autossh for tunnel
    export SSHPASS
    LANG=C nohup sshpass -P "assphrase" -e autossh $ssh_generalopts $ssh_tunnelopts -N -t -t -x $ssh_opts >/dev/null &
    unset SSHPASS
    sleep 5

    # start turbovnc client
    if [ "$START_VNC" == 1 ]; then
	nohup "$vncviewer" -Password="${turbovnc_password}" localhost:"$turbovnc_local_display" >/dev/null &
    fi

    turbovnc_local_display=$(( turbovnc_local_display + 1 ))

    # start sshfs
}



#### CHECK CONDITIONS

log "Make sure everything is available"
for tool in pidof jq nc sudo skill autossh envsubst sshpass setxkbmap wg-quick; do
    command -v "${tool}" >/dev/null || { echo "*** please install ${tool}" >&2; exit 1; }
done


if [ "$START_VNC" == 1 ]; then
    if [ ! -x "$vncviewer" ]; then
	echo "*** please install $vncviewer" >&2
	exit 1
    fi
fi


log "Check if another instance of this script is running"
pidof -o %PPID -x $0 >/dev/null && log "ERROR: Script $0 already running" && exit 1


# JSON Validate the config file
jq . < "${secrets_dir}"/wakeup_conf.json >/dev/null || { log "Bad config"; exit 1; }



if [ "$MODE" != terminate ]; then
    log "Wait for working internet"
    while ! nc -w 1 -v -v -z duckduckgo.com 443 >/dev/null 2>&1; do echo -n "." >&2; sleep 1; done
fi


if [ "$START_VNC" == 1 ]; then
    export JAVA_HOME=`type -p java|xargs readlink -f|xargs dirname|xargs dirname`
    log "Check java (JAVA_HOME=$JAVA_HOME)"
    if [ -z "$JAVA_HOME" ] || [ ! -d "$JAVA_HOME" ]; then
	log "Make sure you have Java installed and configured in ${secrets_dir}/wakeup_conf.json ($JAVA_HOME)" ; exit 1
    fi
fi



#### PROCESS MODES

if [ "$MODE" = help ]; then
    cat <<EOF >&2


$0 - manage autossh and turbovnc couples

Default operation is to initiate connection.


Usage:
 $0 ( -x | -k | -X | -s | -d | -l | -h ) ( -- hostname )


Supply a hostname to connect to it.

Optional flags:
   -k|-x       - terminate connection
   -X          - do not start turbovnc
   -s          - show status for connection
   -d          - debug output
   -l          - list hosts
   -h          - show help
EOF

    exit 0
fi



if [ "$MODE" = terminate ]; then
    kill_vnc
    skill autossh
    wgconf="$(jq -r .wireguard_conf ${secrets_dir}/wakeup_conf.json)"
    sudo wg-quick down "${wgconf/#\~/$HOME}" || true
    exit 0
fi



if [ "$MODE" = list ]; then
    for key in $(jq -r '.hosts | keys[]' < "${secrets_dir}"/wakeup_conf.json); do
	echo "$key"
    done
    exit 0
fi



if [ "$MODE" = status ]; then
    ps u | grep -E 'autossh' >&2
    sudo wg show | grep "listening port" >&2
    exit 0
fi



#### HANDLE VPN

if [ $(jq -r .use_wireguard < "${secrets_dir}"/wakeup_conf.json) = "true" ]; then
    command -v wg-quick >/dev/null || { echo "*** please install wireguard" >&2; exit 1; }
    wgconf="$(jq -r .wireguard_conf ${secrets_dir}/wakeup_conf.json)"
    wgconf="${wgconf/#\~/$HOME}"

    if [ -z "$wgconf" ] || [ ! -f "$wgconf" ]; then
        echo "*** configure wireguard_conf" >&2; exit 1
    fi
    log "Start wireguard"
    sudo wg-quick down "$wgconf"  || true
    sleep 1
    sudo wg-quick up "$wgconf"

    log "Wait for vpn"
    internal_ip=$(jq -r .hosts[].host < $secrets_dir/wakeup_conf.json | head -n 1)
    while ! nc -w 1 -z "${internal_ip}" 22 >/dev/null 2>&1; do echo -n "." >&2; sleep 1; done; echo >&2
else
    log "Skipping vpn"
fi




#### HANDLE DIRECT SSH CONNECTION

if [ -z "$START_VNC" ]; then
    if [ -n "$HOST" ]; then
        host=$(jq -r .hosts.'"'$HOST'"'.host < "${secrets_dir}"/wakeup_conf.json)
        ssh_opts=$(jq -r .hosts.'"'$HOST'"'.ssh_opts < "${secrets_dir}"/wakeup_conf.json)
        SSHPASS=$(jq -r .hosts.'"'$HOST'"'.ssh_passphrase < "${secrets_dir}"/wakeup_conf.json)

        # substitute $host and $turbovnc_local_display
        export host
        ssh_opts="$(echo $ssh_opts | envsubst)"

        # start autossh for tunnel
        export SSHPASS
        export LANG=C
        exec sshpass -P "assphrase" -e ssh $ssh_generalopts $ssh_opts
        exit 0
    else
        echo "supply host if you supply -X" >&2; exit 1
    fi
fi



#### HANDLE SSH PORT FORWARDING AND OPTIONALLY VNC

log "Kill existing processes"
skill autossh

if [ "$START_VNC" == 1 ]; then
    kill_vnc
fi



if [ -n "$HOST" ]; then
    connect_host "$HOST"
else
    for key in $(jq -r '.hosts | keys[]' < "${secrets_dir}"/wakeup_conf.json); do
        connect_host "$key"
    done
fi

#setxkbmap -layout "se(nodeadkeys)"
