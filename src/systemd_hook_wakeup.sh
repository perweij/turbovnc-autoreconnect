#!/usr/bin/env bash
set -eEu -o pipefail
PATH=/sbin:/usr/sbin:/bin:/usr/bin


# Set to the user that uses the laptop's desktop environment. Instantiated by `make install`.
RUNUSER="$CONF_RUNUSER"


# Set to the directory containing wakeup.sh. Instantiated by `make install`.
SCRIPTDIR="$CONF_SCRIPTDIR"


case "$1" in
    post)   I3SOCK=$(ls -1 /run/user/$(id -u "$RUNUSER")/i3/ipc-socket.* | head -n 1)
	    i3-msg -s "$I3SOCK" "exec $SCRIPTDIR/wakeup.sh"
    ;;
esac
