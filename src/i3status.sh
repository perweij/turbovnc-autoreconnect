#!/bin/bash


i3status | while : 
do
    read line
    if [[ "$line" =~ \]$ ]]; then
	STATUS=""
	if pidof -o %PPID -x openfortivpn >/dev/null; then
	    STATUS='{"name":"vpn","color":"#00FF00","markup":"none","full_text":"VPN"},'
	else
	    STATUS='{"name":"vpn","color":"#FF0000","markup":"none","full_text":"no VPN"},'
	fi
	
	if pidof -o %PPID -x autossh >/dev/null; then
	    STATUS+='{"name":"autossh","color":"#00FF00","markup":"none","full_text":"autossh"},'
	else
	    STATUS+='{"name":"autossh","color":"#FF0000","markup":"none","full_text":"no autossh"},'
	fi

	if nr=$(jps | grep -iv " [j]ps$" | wc -l) && [ "$nr" -gt 0 ]; then
	    STATUS+='{"name":"vnc","color":"#00FF00","markup":"none","full_text":"VNC: '$nr'"},'
	else
	    STATUS+='{"name":"vnc","color":"#FF0000","markup":"none","full_text":"no VNC"},'
	fi

	if [[ "$line" =~ ^, ]]; then
	    echo -n ','
	fi
	echo -n '['"$STATUS"
	echo "$line" | sed 's|^[,]*\[||'
    else
	echo "$line"
    fi
done
